# Découvrir l'OSINT : L'Art de l'Intelligence Open Source

L'OSINT ou « ***Open Source Intelligence*** » signifie en français « ***Renseignement de Source Ouverte*** ». Il s'agit d'une information accessible à tous et non classifiée. L'Open Source Intelligence est un élément fondamental pour les opérations de renseignements.
L'investigateur qui recueille ce type de sources est appelé **osinteur**. Le développement de cette pratique permet d'observer l'existence d'une communauté OSINT.

Contrairement aux représentations communes, le renseignement ne traite pas que des informations secrètes ou dissimulées, ce n'est pas toujours le cas puisque beaucoup d'informations essentielles au processus d'intelligence peuvent être trouvées dans l'espace public, ou peuvent être obtenues par des moyens non conventionnels. L'explosion de l'information augmente le nombre de sources accessible aux analystes. Même une action secrète a souvent des conséquences qui ne le sont pas ; il est donc parfois possible aux analystes de rassembler une image d'une activité cachée à partir de diverses sources ouvertes.

Dans le ***renseignement en sources ouvertes***, la collecte d'informations diffère de celle d'autres disciplines du renseignement. Dans ces dernières, obtenir des données brutes pour analyse peut être difficile, surtout auprès de sources non coopératives. En **OSINT**, le défi principal réside dans l'identification de sources pertinentes et fiables parmi la masse d'informations publiques disponibles. Cependant, récupérer les données en elles-mêmes est plus facile car elles sont accessibles publiquement.

Le renseignement de source humaine (overt HUMINT) est parfois considéré comme une composante de l'**OSINT**. Il implique l'utilisation de sources d'information humaines non secrètes, telles que l'interrogation de réfugiés, les entretiens avec des travailleurs légaux, ou la consultation de rapports publics d'agents diplomatiques tels que les attachés et les ambassadeurs.

## L'Open Source Intelligence dans la guerre d'Ukraine

> "Les agences gouvernementales semblent avoir perdu leur monopole du renseignement. De simples utilisateurs de Twitter se sont révélés de fins détectives, suivant les mouvements des troupes, recueillant des preuves sur les crimes de guerre et livrant au grand jour un combat contre la désinformation, [...]. L'Open Source Intelligence n'a pas été inventé en Ukraine, mais ce conflit l'a assurément établi comme une nouvelle force impossible à ignorer."  
**SOPHIE PERROT.**

# Les sources de l'OSINT peuvent être divisées en six catégories différentes de flux d'informations

- Les médias, journaux imprimés, magazines, radios, chaînes de télévision dans les différents pays

- Internet, les publications en ligne, les blogs, les groupes de discussion, les médias citoyens, YouTube et autres réseaux sociaux

- Les données gouvernementales, rapports, budgets, auditions, annuaires, conférences de presse, sites web officiels et discours. Ces informations proviennent de sources officielles, mais sont bien publiquement accessibles et peuvent être utilisées librement et gratuitement

- Les publications professionnelles et académiques, provenant de revues académiques, conférences, publications et thèses

- Les données commerciales, imagerie satellite, évaluations financières et industrielles et bases de données

- La littérature grise, rapports techniques, prépublications, brevets, documents de travail, documents commerciaux, travaux non publiés et lettres d'information


# Comment mettre en place facilement une démarche d'OSINT

## Définissez vos objectifs :
Avant de commencer, identifiez clairement ce que vous recherchez. Que ce soit pour la sécurité de votre entreprise, une enquête journalistique, une veille concurrentielle ou autre, avoir des objectifs précis vous guidera dans la collecte d'informations.

## Identifiez les sources pertinentes :
Pour chaque objectif, identifiez les sources d'informations en ligne qui pourraient contenir les données dont vous avez besoin. Les moteurs de recherche, les médias sociaux, les bases de données publiques et les sites web spécialisés sont souvent des points de départ utiles.

## Utilisez des outils adaptés :
Il existe de nombreux outils et logiciels conçus pour faciliter la collecte et l'analyse d'informations en OSINT. Certains sont gratuits, tandis que d'autres sont payants. Des outils de recherche avancée sur Google aux plates-formes de gestion de médias sociaux et aux agrégateurs d'actualités, choisissez ceux qui correspondent le mieux à vos besoins.

## Restez organisé :
La quantité d'informations collectées peut être écrasante. Utilisez des outils de gestion de projet ou de gestion des informations pour organiser vos résultats et suivre les progrès.

## Soyez éthique et respectez la loi :
Lors de votre recherche, assurez-vous de respecter la vie privée et la légalité. Évitez de collecter des informations personnelles sensibles sans autorisation, et suivez les lois locales sur la collecte de données.

## Analysez les données :
Une fois que vous avez collecté des informations, analysez-les pour en extraire des insights pertinents. Vous pouvez utiliser des techniques d'analyse de données, de visualisation ou simplement une lecture critique pour comprendre ce que les informations révèlent.

## Mettez en œuvre les résultats :
Enfin, utilisez les résultats de votre recherche OSINT pour prendre des décisions éclairées, élaborer des stratégies ou résoudre des problèmes.

## Conclusion
Il est important de noter que l'**OSINT** est une discipline qui exige de la patience, de la pratique et de la formation continue pour être maîtrisée. De plus, elle doit être réalisée de manière éthique et légale. Assurez-vous de rester à jour sur les lois et les réglementations en vigueur dans votre région concernant la collecte et l'utilisation d'informations en ligne.

# Dix exemples d'utilisation de l'OSINT dans la vie quotidienne
1. **Recherche de personnes disparues :**
   - **Exemple d'outil :** Utilisation de moteurs de recherche, réseaux sociaux
   - **Commande :** Utilisez des moteurs de recherche pour rechercher des informations sur des personnes disparues en utilisant leur nom, leurs photos, ou d'autres détails pertinents.
   - **Résultat :** Vous pouvez trouver des indices sur leur dernière localisation, leurs amis ou leurs activités récentes.

2. **Analyse des menaces en ligne :**
   - **Exemple d'outil :** Maltego, Social-Searcher
   - **Commande :** Utilisez Maltego pour cartographier les relations en ligne d'une personne ou d'une organisation, ou utilisez Social-Searcher pour surveiller les discussions sur les médias sociaux liées à des menaces potentielles.
   - **Résultat :** Vous obtenez une visualisation des liens entre les acteurs et des informations sur les menaces émergentes.

3. **Veille sur la concurrence :**
   - **Exemple d'outil :** Google Alerts, SimilarWeb
   - **Commande :** Configurez des alertes Google pour les noms de vos concurrents ou utilisez SimilarWeb pour suivre le trafic et les sources de trafic de leurs sites web.
   - **Résultat :** Vous pouvez surveiller les activités de vos concurrents, leurs stratégies marketing et leur présence en ligne.

4. **Recherche d'emplois :**
   - **Exemple d'outil :** LinkedIn, Indeed
   - **Commande :** Utilisez LinkedIn pour rechercher des offres d'emploi et des entreprises qui recrutent dans votre domaine, ou utilisez Indeed pour trouver des offres d'emploi locales.
   - **Résultat :** Vous obtenez des opportunités d'emploi pertinentes et des informations sur les employeurs.

5. **Investigation journalistique :**
   - **Exemple d'outil :** Wayback Machine, recherches d'archives
   - **Commande :** Utilisez la Wayback Machine pour accéder à des versions archivées de sites web, ou effectuez des recherches d'archives pour trouver des articles ou des documents anciens.
   - **Résultat :** Vous pouvez découvrir des informations historiques ou des preuves pertinentes pour votre enquête.

6. **Évaluation des risques pour les voyages :**
   - **Exemple d'outil :** Google Maps, OSAC (Overseas Security Advisory Council)
   - **Commande :** Utilisez Google Maps pour rechercher des avis sur des lieux de voyage ou consultez les rapports de sécurité d'OSAC pour évaluer les risques dans une région spécifique.
   - **Résultat :** Vous obtenez des informations sur la sécurité et les conseils pour voyager en toute sécurité.

7. **Analyse de l'opinion publique :**
   - **Exemple d'outil :** Twitter Advanced Search, Brandwatch
   - **Commande :** Utilisez Twitter Advanced Search pour suivre les discussions sur un sujet particulier, ou utilisez des outils comme Brandwatch pour analyser les tendances des médias sociaux.
   - **Résultat :** Vous obtenez des informations sur les opinions et les réactions du public à un événement ou à une question.

8. **Recherche d'informations juridiques :**
   - **Exemple d'outil :** Bases de données juridiques en ligne
   - **Commande :** Utilisez des bases de données juridiques en ligne pour rechercher des cas de jurisprudence, des lois et des règlements.
   - **Résultat :** Vous accédez à des informations juridiques pour soutenir une affaire ou une recherche juridique.

9. **Analyse de la cybersécurité :**
   - **Exemple d'outil :** Shodan, VirusTotal
   - **Commande :** Utilisez Shodan pour rechercher des dispositifs connectés à Internet, ou utilisez VirusTotal pour analyser des fichiers suspects.
   - **Résultat :** Vous identifiez des vulnérabilités potentielles ou des menaces en ligne.

10. **Recherche académique :**
    - **Exemple d'outil :** Google Scholar, ResearchGate
    - **Commande :** Utilisez Google Scholar pour trouver des articles académiques ou utilisez ResearchGate pour accéder aux publications de chercheurs.
    - **Résultat :** Vous obtenez des références académiques pour vos travaux de recherche.

N'oubliez pas que nous avons une section dédiée sur [les outils utilisés]() en OSINT où vous pouvez trouver plus d'informations sur les outils spécifiques et leurs utilisations.  

# Les actualités dans le monde de l'OSINT

Les faits d'actualité démontrent régulièrement la puissance de l'OSINT dans le domaine du renseignement et de la découverte d'informations sensibles. Un exemple récent de cette capacité est **la localisation de la base souterraine secrète iranienne**, baptisée Aigle-44. Au début du mois de février, l'Armée de l'air iranienne avait diffusé des images de cette nouvelle installation, mettant en lumière l'aspect stratégique de cette base. Cependant, grâce à l'analyse minutieuse du renseignement d'origine sources ouvertes (OSINT), des experts ont rapidement réussi à déterminer l'emplacement exact de cette base secrète. Cette démonstration met en évidence la manière dont l'OSINT peut être utilisé pour ***extraire des informations cruciales à partir de sources publiques***, démontrant ainsi son rôle essentiel dans la compréhension des événements mondiaux et des enjeux géopolitiques actuels.

# OSINT-FR : La Communauté de l'OSINT

**OSINT-FR** est une communauté cosmopolite, rassemblant des passionnés et des curieux désireux de développer leurs compétences en Renseignement d’Origine Source Ouverte (ROSO) ou Open Source Intelligence (OSINT).

Créée en janvier 2019 par Hugo Benoist (HuGe) et Sylvain Hajri (navlys_), cette communauté à vocation d’éducation populaire réunit des membres de toutes nationalités et professions, partageant un intérêt commun pour l'OSINT.

Linguistes, consultants en cybersécurité, journalistes, enquêteurs privés, analystes en intelligence économique, juristes, experts financiers, et bien d'autres, les membres proviennent d'horizons divers et mettent à profit leurs compétences et expertises.

**Ce qu'ils font :**
- Ils développent des outils
- Ils partagent des méthodologies
- Ils mènent des enquêtes

De plus, cette communauté réalise une veille collaborative dans des domaines variés tels que la géopolitique, l'IMINT/GEOINT, le SIGINT, le SOCMINT, l'OPSEC, le Darkweb, la Cyber Threat Intelligence (CTI), la désinformation, et bien d'autres encore.

**Événements :** OSINT-FR organise régulièrement des conférences, des ateliers et des rencontres, avec des intervenants de renommée internationale, pour promouvoir l'éducation populaire et le partage de connaissances dans le domaine de l'OSINT.

**Membres :** Plus de 11 000 membres sont actuellement dans cette communauté.

Pour en savoir plus sur les outils utilisés en OSINT et d'autres informations, n'hésitez pas à visiter leurs [site web](https://osintfr.com/fr/accueil/).
# Les Outils utilisés Dans l'OSINT

Pour faire de l'OSINT, il est intéressant et nécessaire d'utiliser certains outils.  

Dans cette partie, nous allons voir différents script et sites utiles afin de réaliser des recherches.  

Ces différents éléments peuvent être catégorisés, nous verrons aussi donc les différentes catégories et leur significations.

- [EMAILS](#emails)
- [Threat Intelligence](#threat-intelligence)
- [Personnes](#personnes)
- [Compagnies](#compagnies)
- [Images](#images)
- [Sites Web](#sites-web)
- [SOCMINT](#social-media-int-socmint)
- [OPSEC](#opsec)
- [Darkweb](#darkweb)
- [Phone Number](#phone-number)
- [Username](#username)
- [GEOINT](#geoint)

## EMAILS  

 Il peut-être extrêment intéressant de connaître les sites auxquelles une adresse mail est lié. En effet ça peut permettre de trouver de nouvelles informations (compte instagram, numéro de téléphone...).  
Pour cela, plusieurs outils existent.  
* [Epios google Finder](https://tools.epieos.com/google-account.php)  
Ce site permet de trouver l'activité lié à un compte google.
* [Holehe](https://github.com/megadose/holehe)  
Ce script en python permet de connaître les différents sites sur lequel est un mail.  
* [h8mail](https://github.com/khast3x/h8mail)  
Ce script permet de faire des recherches autour d'un mail ou d'un mot de passe.  

## Threat Intelligence
Le travail sur autour des entreprises est très utiles afin de réaliser des connexions avec des personnes.  
* [CRT.sh](https://crt.sh/)  
Ce site permet de réaliser des recherches sur les empreintes de certificats et les éléments similaires.  
* [Onyphe](https://www.onyphe.io/)  
C'est un moteur de rechercher qui collecte le données des @IP et des services.
* [Shodan](https://www.shodan.io/)  
C'est un moteur qui permet d'explorer les services et @IP ouvertes dans le monde.  

## Personnes  
Lorsque qu'on fait de l'OSINT sur une personne (personnes disparues, criminels...) avoir des outils permettant de faire spécfique des recherches sur les personnes est indispensable.  
* [Hunter.io](https://hunter.io/)  
Ce premier site permet de trouver le mail professionnel d'une personne.  
* [Lusha](https://www.lusha.com/)
Ce site permet de récupérer des informations à partir des médias sociaux.  
## Compagnies
Les documents des compagnies sont certainements les informations les plus utiles mais aussi des fois les plus dures à avoir. Grâce à ces outils, vous pourrez plus facilement les trouver.
* [OCCRP Aleph](https://data.occrp.org/)  
C'est un moteur de recherche qui permet de trouver des fichier mais aussi les fuites de données.
* [Offshore Data LEAKS](https://offshoreleaks.icij.org/)
C'est une base de donnée permettant de connaître les personnes derrières les sociétés offshore.
* [Opencorporate](https://opencorporates.com/)
C'est la plus grande base de données ouvertes sur les entreprises.
* [Société Ninja](https://societe.ninja/)
C'est une base de données des informations publiques et gratuites des entreprises françaises.
## Images
En OSINT, les images et les vidéos sont les éléments les plus importants et les plus partagés. Il est donc normal d'avoir des outils dédiés à cela.
* [FindClone](https://findclone.ru/)  
C'est un site permettant de réaliser de la reconnaissance faciale.  
* [Forensically](https://29a.ch/photo-forensics/)  
C'est un analyseur gratuit en ligne.  
* [Invid](https://www.invid-project.eu/)
C'est un ensemble de logiciel permettant d'extraire des images de vidés et d'obtenir des informations.  
* [PimpEyes](https://pimeyes.com/)
C'est un site permettant de réaliser des recherches de reconnaissance faciale.
## Sites Web
Lorsqu'on a un site il est parfois diffcile d'avoir des informations. Cependant grâce à ces différents sites plus de problèmes.
* [Domain Big Data](https://domainbigdata.com/)  
Ce site permet de trouvez le registrant et les domaines appartenant à une personne grâce à un whois inversé
* [Rapid DNS](https://rapiddns.io/)  
Outil d’interrogation DNS qui permet d’interroger facilement les sous-domaines ou les sites d’une même IP.
* [Spiderfoot](https://www.spiderfoot.net/)  
Avec plus de 200 modules pour la collecte et l’analyse des données, vous aurez la vue la plus complète de la surface d’attaque de votre organisation sur Internet.
* [urlscan.io](https://urlscan.io/)  
Scanner de sites web pour détecter les URL suspectes et malveillantes.
* [Wayback Machine]()  
Ce site permet d'explorer les 515 milliards de pages web sauvegardées au fil du temps.
## Social Media INT (SOCMINT)
* [InstaLocTrack](https://github.com/bernsteining/instaloctrack)  
Ce script permet de trouver les géolocalisations de publications instagram
* [SnapMap](https://map.snapchat.com/)  
C'est une carte permettant de géolocaliser les dernières stories publiques.
* [Tweets Analyzer](https://github.com/x0rz/tweets_analyzer)  
Ce script permet de corréler les données provenant de messages ou de comptes Twitter.
* [Twint](https://github.com/twintproject/twint)  
Ce Script permet de réaliser du scraping et de l'OSINT sur Twitter.
## OPSEC
L'un des éléments importants lors de l'OSINT c'est de savoir se cacher des gens. Particulièrement lorsqu'on à faire à des criminels.
* [FakeNameGenerator](https://www.fakenamegenerator.com/)  
Ce site permet de générer une fausse identité en ligne.
* [ThisPersonDoesNotExist](https://thispersondoesnotexist.com/)  
Ce site permet de générer des fausses images de personnes en utilisant un réseau neuronal.
* [TinyCheck](https://github.com/KasperskyLab/TinyCheck)  
Ce script permet de capturer facilement les communications réseau d'un appareil associé à un AP et de les analyser.

## Darkweb
Faire des recherches sur le darknet est assez compliqué car il est nécessaire d'installer différents logiciels mais grâce à se script plus besoin.

* [OnionSearch](https://github.com/megadose/OnionSearch)

## Phone Number
Les numéros de téléphones sont de super outils pour retrouver des éléments et des personnes.

* [Epieos Phone Number](https://tools.epieos.com/google-account.php)  
Ce site permet de retrouver les comptes liés à un numéro de téléphone.  
* [PhoneInfoga](https://github.com/sundowndev/PhoneInfoga)  
Ce script permet de collecter des informations sur des numéros de téléphone.
* [TrueCaller](https://www.truecaller.com/)  
Permet d'identifier et de bloquer les appels

## Username
Les usernames sont la bases d'internet, il est donc important de pouvoir les recherches de façon simple et rapide.

* [Maigret](https://github.com/soxoj/maigret)  
C'est un outil hors ligne permettant de rechercher un username sur de nombreux site web.  
*[WhatsMyName](https://whatsmyname.app/)  
C'est un outil en ligne qui permet de chercher un nom d'utilisateur.  

## GEOINT
L'un des éléments les plus dure à faire en OSINT c'est la GEOINT. C'est à dire de retrouver la position d'un élément.

* [GeoInt Localisator](https://github.com/Th0rr/GeoInt-Localisator)  
C'est un script qui permet de trouver la distance entre deux lieux génériques.  
* [Overpass turbo](https://overpass-turbo.eu/)  
C'est un outil de filtrage de données basé sur OpenStreetMap
* [SunCalc](https://www.suncalc.org/)  
Application en ligne avec carte in